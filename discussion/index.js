console.log("Discussion");

/*JavaScript-Function Parameters and Retun Statements*/


//Function able to receive data without the use of global variable or prompt();
// name - is a parameter

// A parameter is a variable/container that existst only in out function and is used to store information that is provided to a function when it is called/invoked.


function printName(name){
	console.log("My name is " + name)
};


// Data passed to a function can be received by the function.
// The value we passed is called an argument.
// The argument is stored in a container called a parameter.
printName("Glenn");
printName("Myrtle");


function printMyAge(age){
	console.log("I am " + age + " years old.");
};

printMyAge(25);
printMyAge();


//Check divisibility reusably using a function with arguments and paramters

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder );
	
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisble by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);

/*
	Mini Activity

	1. Create a function which is able to receive data as an argument:
		- This function should be able to receive the name of your favorite superhero
		- Display the name of your favorit superhero in the console

*/	
	//code is here:

	function getSuperHero(superHero){
		console.log("Your favorite superhero is: " + superHero);
	};

	// let mySuperHero = prompt("Enter your favortite super hero.");
	// getSuperHero(mySuperHero);

/*

	2. Create a function which determine the input number is even number.
		- This function should be able to receive any number
		- Display the number and state if even numbers.	
	

*/

	//code is here:

	function determineEvenNumber(number){
		let remainder = number % 2;
		console.log("The number you entered is " + number);

		let isEvenNumber = remainder === 0;
		console.log("Is " + number + " an even number?")
		console.log(isEvenNumber);

	};

	// let enterNumber = prompt("Enter a number");
	// determineEvenNumber(enterNumber);



	// AFTER MINI ACTIVITY

	// Multiple Arguments can also be passed into a function; multiple parameters can contain our arguments.



	function printFullName(firstName, middleInitial, lastName){
		console.log(firstName + ' ' + middleInitial + ' ' + lastName);
	};


	printFullName('Juan', 'Crisostomo', 'Ibarra');
	printFullName('Ibarra', 'Juan', 'Crisostomo');

/*
	Parameters will contain the argument according to the order it was passed

	In other language, providing more/less arguments than the expected parameters sometime causes an errot or changes the behavior of the function

 */

printFullName('Stephen', 'Wardell');
printFullName('Stephen', 'Wardell', 'Curry', 'Thomas');
printFullName('Stephen', 'Wardell', 'Curry');
printFullName('Stephen', ' ', 'Curry');

// Use variables as arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName, mName, lName);

/* Mini - Activity
	Create a function which will be able to receive 5 arguments
		- Receive 5 of your favorite songs
		- Display/print the passed 5 favorite songs in the console when the function is invoked.

*/

//code here:
	console.log("Mini Activity");


	function displayTopFiveSongs(firstSong, secondSong, thirdSong, fourthSong, fifthSong){
		console.log("My Top 5 Songs are:")
		console.log("1. " + firstSong);
		console.log("2. " + secondSong);
		console.log("3. " + thirdSong);
		console.log("4. " + fourthSong);
		console.log("5. " + fifthSong);
	};

	displayTopFiveSongs('To Have and To Hold', 'Mirror', 'Iris', '214', 'Mahika');



	//RETURN STATEMENT
	//Currently or so far, or dunction are able to display data in our console.
	//However, our function cannot yet return values. Functions are able to return values which can be saved into a variable using the return statement/keyword

	let fullName = printFullName('Glenn', 'A', "Perey");

	console.log(fullName);

	function returnFullName(firstName, middleInitial, lastName){
		return firstName + ' ' + middleInitial + ' ' + lastName;
	};

	fullName = returnFullName('Myrtle', 'A', 'Perey');
	//printFullName(); return undefined because the function does not have return statement.
	//returnFullName(); return a string as a value because it has a return statement;
	console.log(fullName);

	//functions which have return statement are able to return value an it can be saved in a variable.


	function returnPhilippineAddress(city){
		return city + ", Philippines";
	};


	let myFullAddress = returnPhilippineAddress("Cavite");

	console.log(myFullAddress);


	//return true if number is divisible by 4, else return false

	function checkDivisibilityBy4(number){
		let remainder = number % 4;
		let isDivisibleBy4 = remainder === 0;

		//returns either true or false
		//not only can you return raw values/data, you can also direturn a value
		return isDivisibleBy4;
		// return keyword not only allows us to return value but also ends the process of the function
		console.log("I am invoked after the return");
	};

	let numIsDivisibleBy4 = checkDivisibilityBy4(4);
	let num14IsDivisibleBy4 = checkDivisibilityBy4(14);


	console.log(numIsDivisibleBy4);
	console.log(num14IsDivisibleBy4);


	function createPlayerInfo(userName, level, job){
		return 'user: ' + userName + " level: " + level + " job: " + job;
	};

	let user1 = createPlayerInfo('white_night', 95, 'Paladin');
	console.log(user1);




// MINI ACTIVITY 3

/*
	Mini-Activity
	Create a function which will be able to add two numbers
		- numbers must be provided as argument
		- display the result of the addition in our console
		- function should only display result. It should not return anything.

		- invoke and pass 2 arguments to the addition function
		- use return

 */

	function add2Numbers(num1, num2){
		return num1 + num2;
	}

	let sum = add2Numbers(5,2);
	console.log("The result is:");
	console.log(sum);






//MINI ACTIVITY 4

/*
	Create a function which will be able to multiply two numbers.
		- Numbers must be provided as arguments
		- Returm the result of multiplication

	Create a global variable called outside of the function called product
		- This product variable should be able to receive and store the result of multiplication function

	Log the value of product variable in the console

 */

	let productGlobal;
	function multiply2Numbers(num1, num2){
		return (num1 * num2);
	};

	productGlobal = multiply2Numbers(2,9);
	console.log("The result is:")
	console.log(productGlobal);




//Activity 5

/*
  Create a function which will be able to get the area of a circle from a provided radius
     - a number should be provided as an argument
     - look up for the formula for calculating the area of a circle 
     - look up for the use of exponent operator
     - you can save the value of the calculations in a variable
     - return the result of the area 

   Create a global variable called outside of the function called circleArea
    - this variable should be able to receive and store the result of the calculation of area of a circle
   Log the value of the circleArea in the console.

 */

	function getAreaOfCircle(pi, radius){
		let area = pi * (radius**2);
		return area;
	}

	
	let circleArea =  getAreaOfCircle(3.1416, 3);

	console.log("The area of a circle with radius 3 is: " + circleArea);





/* =====================
 Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */

	console.log("ASSIGNMENT OUTPUT");

	function checkGrade(myScore, totalScore){
		let percentageScore = (myScore/totalScore) * 100;
		let isPassed = percentageScore > 75;

		return isPassed;
	};

	let isPassingScore = checkGrade(77,80);
	console.log("Is my score on the passing rate of 75%?");
	console.log(isPassingScore);